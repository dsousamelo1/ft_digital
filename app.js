var express = require('express'); 
var bodyParser = require('body-parser');
const AssistantV1 = require('watson-developer-cloud/assistant/v1');
var cfenv = require('cfenv');
const fs = require('fs');
var appEnv = cfenv.getAppEnv();
app = express();

app.use(express.static('./public')); 
app.use(bodyParser.json());

const assistant = new AssistantV1({
  iam_apikey: 'uZaBERfqv57oaiFKuk6XlJMH_UGEBKlwIXvaRnr33szn',
  version: '2018-09-20',
  url: 'https://gateway.watsonplatform.net/assistant/api'
});


var dataList = new Array();


var rawdata = fs.readFileSync('serviços.json'); 


var dataset = JSON.parse(rawdata); 

function searchValues(needle) {
    
    let found = [];
    var re = new RegExp(needle, 'i');
    dataset.forEach(function(item, ix) {
      Object.keys(item).forEach(function(key) {
        if (typeof item[key] !== 'string') return;
        if (item[key].match(re)) {
          if (found.indexOf(ix) === -1) { found.push(ix); }
        }
      });
    });
    return {searched: needle, indexes:found};
  }

 

app.post('/conversation/', (req, res) => {
  const { text, context = {} } = req.body;

  const params = {
    input: { text },
    workspace_id:'cae4db5f-1c7a-419d-925a-44b19a1a700b',
    context,
  };
  
  assistant.message(params, (err, response) => {
    if (err) {
      console.error(err);
      res.status(500).json(err);
    } else {
      console.log(response.output.text[0]);
      console.log('INTENT: '+response.intents.length);
      console.log('ENTITIES: '+response.entities.length);

      if(response.entities.length == 1){

          let valor = response.entities[0].value;

          let found = searchValues(valor);

          let obj = dataset[found.indexes[0]];  
         
          //console.log(obj);

          if(response.intents.length == 0){
            dataList.push(obj.intents);
            dataList.push(response);
            res.json(dataList);
            dataList = [];
          }else if(obj){
            dataList.push(obj.intents);
            dataList.push(response);
            //console.log(response.intents[0].intent)
            dataList.push(response.intents[0].intent);
            console.log(dataList[2]);
            res.json(dataList);
            dataList = [];
          }else if(!obj){
            res.json(response);
          }
     
      }else{
        res.json(response);
      }
      
    }
  });
});

// start server on the specified port and binding host
app.listen(appEnv.port, '0.0.0.0', function() {
  // print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
});

