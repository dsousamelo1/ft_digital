
var worker = new Worker("js/timer.js");
flag = '';
cont = 0;
isShow = true;

worker.postMessage("iniciar");

worker.addEventListener('message', function(e) {

      horario_atual = JSON.parse(e.data)
      horas = horario_atual.hora;
      minutos = horario_atual.minuto;

      
        if(horas>='18' && horas <='5'){
          $("fundo").css("background","#03a9f4");
          $("usuario").attr("src","./img/icon/icon-user-noite.png");
          $("luz").attr("src","./img/luz/icon-luz-aberta-noite.png");
          $("porta").attr("src","./img/porta/icon-porta-aberta-noite.png");

          if(cont >= 25){
            $("#sol").attr("src","./img/sunny/sunny-bocejando.gif");
            cont = 0;
            flag = '';
          } 
          else{
             cont++;
            if(horas == '16' && minutos == '20'){
              if(flag != "comendo_flag"){
                flag = "comendo_flag";
                $("#sol").attr("src","./img/sunny/sunny-comendo.gif");
              }
            } 
            else {
              if(flag != "idol_flag"){
                flag = "idol_flag";
                $("#sol").attr("src","./img/sunny/sunny-idol.gif");
               }
            }
          }
                      
        }
        else{
          $("usuario").attr("src","./img/icon/icon-user-dia.png");
          $("luz").attr("src","./img/luz/icon-luz-aberta-dia.png");
          $("porta").attr("src","./img/porta/icon-porta-aberta-dia.png");

          if(cont >= 25){
              $("#sol").attr("src","./img/sunny/sunny-bocejando.gif");
              cont = 0;
              flag = '';
          } 
          else{
              cont++;
              if(horas == '16' && minutos == '20'){
                if(flag != "comendo_flag"){
                  flag = "comendo_flag";
                  $("#sol").attr("src","./img/sunny/sunny-comendo.gif");
                }
              } 
              else {
                if(flag != "idol_flag"){
                  flag = "idol_flag";
                  $("#sol").attr("src","./img/sunny/sunny-idol.gif");
                }
              }
            }
          }           
        
}, false);

function button_animation() {
    if (isShow) {
        isShow = false;
        $('#animation').attr('class', 'animated bounceOutRight');
    } else {
        isShow = true;
        $('#animation').attr('class', 'animated bounceInRight');
    }
}
