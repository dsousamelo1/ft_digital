function getState(){

  fetch('https://sunny-57c58.firebaseio.com/temp.json')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }
      
      // Examine the text in the response
      response.json().then(function(data) {
        //console.log(data);
        postMessage(data);
      });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });

  setTimeout("getState()", 1500);
}

self.addEventListener('message', function(e) {
  getState();
}, false);