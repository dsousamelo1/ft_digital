const textInput = document.getElementById('input');

let context = {};

function enviarMensagem() {
  if (textInput.value) {
    $(".ms-body").append('<div class="message-feed right"><div class="pull-right"><img src="img/user.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + textInput.value + '</div><small class="mf-date"><i class="fa fa-clock-o"></i> ' + Date() + '</small></div></div>');
    scroll();
    getWatsonMessageAndInsertTemplate(textInput.value);       
    textInput.value = '';

  }else{
    alert("É nescessário escrever alguma coisa para conversar com o Sunny")
  }
}


const getWatsonMessageAndInsertTemplate = async (text = '') => {  

  const uri = '/conversation';
  console.log(uri);
  const response = await (await fetch(uri, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      text,
      context,
    }),
  })).json();
 
  let resposta;

  let intents = new Array();

  intents = response[0];

  let desc = intents[0].resposta;
  let req = intents[1].resposta;
  let eta = intents[2].resposta;
  //let link = intents[3].resposta;
  let help = intents[4].resposta;
  let hora = intents[5].resposta;
  let ender = intents[6].resposta;
  let area = intents[7].resposta;

  let descN = intents[0].nome;
  let reqN = intents[1].nome;
  let etaN = intents[2].nome;
  //let link = intents[3].resposta;
  let helpN = intents[4].nome;
  let horaN = intents[5].nome;
  let enderN = intents[6].nome;
  let areaN = intents[7].nome;

  resposta = response[1].output.text;  

  context = response[1].context;

  if(response.length > 2){
    if(descN == response[2]){
      $(".ms-body").append('<div class="message-feed media"><div class="pull-left"><img src="img/watson.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + resposta+'<br>'+desc+'</div></div>');
    }
    if(reqN == response[2]){
      $(".ms-body").append('<div class="message-feed media"><div class="pull-left"><img src="img/watson.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + resposta+'<br>'+req+'</div></div>');      
    }
    if(etaN == response[2]){
      $(".ms-body").append('<div class="message-feed media"><div class="pull-left"><img src="img/watson.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + resposta+'<br>'+eta+'</div></div>');
    }
    if(helpN == response[2]){
      $(".ms-body").append('<div class="message-feed media"><div class="pull-left"><img src="img/watson.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + resposta+'<br>'+help+'</div></div>');
    }
    if(horaN == response[2]){
      $(".ms-body").append('<div class="message-feed media"><div class="pull-left"><img src="img/watson.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + resposta+'<br>'+hora+'</div></div>');
    }
    if(enderN == response[2]){
      $(".ms-body").append('<div class="message-feed media"><div class="pull-left"><img src="img/watson.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + resposta+'<br>'+ender+'</div></div>');
    }
    if(areaN == response[2]){
      $(".ms-body").append('<div class="message-feed media"><div class="pull-left"><img src="img/watson.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + resposta+'<br>'+area+'</div></div>');
    }
    scroll();
  }else{

    $(".ms-body").append('<div class="message-feed media"><div class="pull-left"><img src="img/watson.png" alt="" class="img-avatar"></div><div class="media-body"><div class="mf-content">' + resposta+'<br>'+desc+'<br>'+req+'<br>'+eta+'<br>'+help+'<br>'+hora+'<br>'+ender + '</div></div>');
    
    
    scroll();
  }
};

function handle(e) {
  if (e.keyCode === 13) {
    enviarMensagem();
    e.preventDefault();
  }
}

getWatsonMessageAndInsertTemplate();