#!/usr/bin/env python3
import Adafruit_DHT
import RPi.GPIO as GPIO
import time
from firebase import Firebase


tempe = Firebase('https://sunny-57c58.firebaseio.com/temp/')
door = Firebase('https://sunny-57c58.firebaseio.com/door/')
light = Firebase('https://sunny-57c58.firebaseio.com/light/')

# Define o tipo de sensor
sensor = Adafruit_DHT.DHT11
#sensor = Adafruit_DHT.DHT22

#configureDigitalPin(ldr,17,'input')
GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.IN)
GPIO.setup(2, GPIO.IN)

# Define a GPIO conectada ao pino de dados do sensor
pino_sensor = 4
 
# Informacoes iniciais
print ("*** Lendo os valores de temperatura e umidade");
 
while(1):
   # Efetua a leitura do sensor
   umid, temp = Adafruit_DHT.read_retry(sensor, pino_sensor);
   
   print(("Temperatura = {0:0.1f}  Umidade = {1:0.1f}n").format(temp, umid));
   
   tempe.put(temp);
   inputLuz = GPIO.input(17)
   inputDoor = GPIO.input(2)
   
   #print(input)
   
   if(inputLuz == 1):
       print("LUZ APAGADA")
       light.put("false")
   if(inputLuz == 0):
       print("LUZ ACESA")
       light.put("true")
   
   print(inputDoor)
   if(inputDoor == 0):
       print("PORTA FECHADA")
       door.put("false")
   if(inputDoor == 1):
       print("PORTA ABERTA")
       door.put("true") 
   
   
